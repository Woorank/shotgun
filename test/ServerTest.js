'use strict';

/* global describe, it, beforeEach, afterEach */

var app = require('../lib/shotgun');
var request = require('supertest');
var express = require('express');
var objectStore = require('../lib/objectStore');
var sinon = require('sinon');
var assert = require('assert');
var fixtureApp;

var putObjectStub = sinon.stub(objectStore, 'put').returns(Promise.resolve());

var logger = require('../lib/logger');
logger.level('fatal');

describe('Server', function () {
  beforeEach(function () {
    putObjectStub.reset();
  });

  describe('General Routes', function () {
    describe('GET /', function () {
      it('should return 404', function (done) {
        request(app)
          .get('/')
          .expect(404, done);
      });
    });

    describe('GET /healthcheck', function () {
      it('should return 200', function (done) {
        request(app)
          .get('/healthcheck')
          .expect('OK')
          .expect(200, done);
      });
    });
  });

  describe('Generation Routes', function () {
    beforeEach(function () {
      fixtureApp = express()
        .use(express.static(`${__dirname}/fixtures/public`))
        .use(app)
        .get('/', function (req, res) {
          res.sendStatus(200);
        })
        .listen(0);
    });

    afterEach(function () {
      fixtureApp.close();
    });

    describe('GET fixture /', function () {
      it('should return 200', function (done) {
        request(fixtureApp)
          .get('/')
          .expect(200, done);
      });
    });

    describe('GET fixture / through loopback', function () {
      it('should return 200', function (done) {
        request('http://127.0.0.1:' + fixtureApp.address().port)
          .get('/')
          .expect(200, done);
      });
    });

    describe('GET fixture /test.html', function () {
      it('should return 200', function (done) {
        request('http://127.0.0.1:' + fixtureApp.address().port)
          .get('/test.html')
          .expect(200, done);
      });
    });

    describe('GET /desktop without url', function () {
      it('should return 400', function (done) {
        request(app)
          .get('/desktop')
          .expect(400, done);
      });
    });

    describe('GET /desktop with immediate (1ms) timeout', function () {
      this.timeout(200);
      it('should return 500', function (done) {
        request(app)
          .get(
            '/desktop?url=' + 'http://127.0.0.1:' + fixtureApp.address().port +
            '&timeout=1'
        )
          .expect(500, done);
      });
    });

    describe('GET /desktop with delay longer than timout', function () {
      this.timeout(1000);
      it('should return 500', function (done) {
        request(app)
          .get(
            '/desktop?url=' + 'http://127.0.0.1:' + fixtureApp.address().port +
            '&timeout=200' +
            '&delay=400'
        )
          .expect(500, done);
      });
    });

    describe('GET /desktop screenshot', function () {
      this.timeout(21000);
      it("should return 200 and Content-Type 'image/png'", function (done) {
        request(app)
          .get(
            '/desktop' +
            '?url=' + 'http://127.0.0.1:' + fixtureApp.address().port +
            '/test.html' +
            '&timeout=20000'
        )
          .expect('Content-Type', 'image/png')
          .expect(200, done);
      });
    });

    describe('GET /iphone screenshot', function () {
      this.timeout(21000);
      it("should return 200 and Content-Type 'image/png'", function (done) {
        request(app)
          .get(
            '/iphone' +
            '?url=' + 'http://127.0.0.1:' + fixtureApp.address().port +
            '/test.html' +
            '&timeout=20000'
        )
          .expect('Content-Type', 'image/png')
          .expect(200, done);
      });
    });

    describe('GET /ipad screenshot', function () {
      this.timeout(21000);
      it("should return 200 and Content-Type 'image/png'", function (done) {
        request(app)
          .get(
            '/ipad' +
            '?url=' + 'http://127.0.0.1:' + fixtureApp.address().port +
            '/test.html' +
            '&timeout=20000'
        )
          .expect('Content-Type', 'image/png')
          .expect(200, done);
      });
    });

    describe('GET /desktop screenshot of unexisting url', function () {
      this.timeout(21000);
      it('should return 500', function (done) {
        request(app)
          .get(
            '/desktop' +
            '?url=http://i.do.not.exist' +
            '&timeout=20000'
        )
          .expect(500, done);
      });
    });

    describe('POST /desktop screenshot', function () {
      it('should return a json with the s3 url', function (done) {
        request(app)
          .post('/desktop')
          .type('application/json')
          .send(JSON.stringify({
            url: 'http://127.0.0.1:' + fixtureApp.address().port + '/test.html',
            timeout: 20000
          }))
          .expect('Content-Type', /json/)
          .expect(200)
          .expect(function (res) {
            assert.strictEqual(
              res.body.location,
              'https://s3.amazonaws.com/screenshots.test.woorank.com/127.0.0.1%3A' +
              fixtureApp.address().port +
              '%2Ftest.html.png'
            );
            assert.strictEqual(putObjectStub.callCount, 1);
            assert.strictEqual(
              putObjectStub.getCall(0).args[0],
              'screenshots.test.woorank.com/127.0.0.1%3A' +
              fixtureApp.address().port +
              '%2Ftest.html.png'
            );
            assert(putObjectStub.getCall(0).args[1] instanceof Buffer, 'Expected a Buffer');
          })
          .end(done);
      });
    });

    describe('POST /ipad screenshot', function () {
      it('should return a json with the s3 url', function (done) {
        request(app)
          .post('/ipad')
          .type('application/json')
          .send(JSON.stringify({
            url: 'http://127.0.0.1:' + fixtureApp.address().port + '/test.html',
            timeout: 20000
          }))
          .expect('Content-Type', /json/)
          .expect(200)
          .expect(function (res) {
            assert.strictEqual(
              res.body.location,
              'https://s3.amazonaws.com/screenshots.test.woorank.com/127.0.0.1%3A' +
              fixtureApp.address().port +
              '%2Ftest.html.ipad.png'
            );
            assert.strictEqual(putObjectStub.callCount, 1);
            assert.strictEqual(
              putObjectStub.getCall(0).args[0],
              'screenshots.test.woorank.com/127.0.0.1%3A' +
              fixtureApp.address().port +
              '%2Ftest.html.ipad.png'
            );
            assert(putObjectStub.getCall(0).args[1] instanceof Buffer, 'Expected a Buffer');
          })
          .end(done);
      });
    });

    describe('POST /iphone screenshot', function () {
      it('should return a json with the s3 url', function (done) {
        request(app)
          .post('/iphone')
          .type('application/json')
          .send(JSON.stringify({
            url: 'http://127.0.0.1:' + fixtureApp.address().port + '/test.html',
            timeout: 20000
          }))
          .expect('Content-Type', /json/)
          .expect(200)
          .expect(function (res) {
            assert.strictEqual(
              res.body.location,
              'https://s3.amazonaws.com/screenshots.test.woorank.com/127.0.0.1%3A' +
              fixtureApp.address().port +
              '%2Ftest.html.iphone.png'
            );
            assert.strictEqual(putObjectStub.callCount, 1);
            assert.strictEqual(
              putObjectStub.getCall(0).args[0],
              'screenshots.test.woorank.com/127.0.0.1%3A' +
              fixtureApp.address().port +
              '%2Ftest.html.iphone.png'
            );
            assert(putObjectStub.getCall(0).args[1] instanceof Buffer, 'Expected a Buffer');
          })
          .end(done);
      });
    });

    describe('POST /desktop without url', function () {
      it('should return 400', function (done) {
        request(app)
          .post('/desktop')
          .type('application/json')
          .send(JSON.stringify({}))
          .expect(400, done);
      });
    });
  });
});
