'use strict';

var express = require('express');
var webshot = require('webshot');
var types = require('./types.js');
var gm = require('gm');
var Promise = require('bluebird');
var objectStore = require('./objectStore');
var bodyParser = require('body-parser');
var logger = require('./logger');
var extend = require('extend');

var SCREENSHOTS_BUCKET = process.env.SCREENSHOTS_BUCKET;

var app = express();

app.use(bodyParser.json());

app.use(function (req, res, next) {
  req.logger = logger.child();
  next();
});

// Healthcheck page
app.get('/healthcheck', function (req, res) {
  res.sendStatus(200);
});

function parseConfig (presets, options) {
  options = options || {};
  return extend({}, presets, {
    shotSize: { width: 'all', height: 'all' },
    timeout: 20000,
    renderDelay: 1000,
    errorIfStatusIsNot200: true,
    onInitialized: {
      fn: require('./prerender/remove-belowfold-images'),
      context: {
        foldHeight: presets.innerHeight
      }
    },
    phantomConfig: {
      'ignore-ssl-errors': 'true',
      'ssl-protocol': 'tlsv1'
    }
  }, options);
}

function getScreenshotFromWebshot (url, config) {
  return Promise.fromCallback(cb => webshot(url, config, cb));
}

function resizeScreenshot (screenshot, config) {
  return Promise.fromCallback(cb => {
    gm(screenshot)
      .resize(config.crop.width)
      .crop(config.crop.width, config.crop.height)
      .resize(config.resize.width, config.resize.height)
      .toBuffer('png', cb);
  });
}

function generateScreenshot (url, config) {
  return getScreenshotFromWebshot(url, config)
    .then(screenshot => resizeScreenshot(screenshot, config));
}

app.post('/:type(iphone|ipad|desktop)', function (req, res, next) {
  var startTime = Date.now();
  var url = req.body.url;
  var type = req.params.type;

  if (url === undefined) {
    return res.status(400).send('Please provide a url');
  }

  var presets = types[type];

  req.logger = req.logger.child({
    url: url,
    type: type
  });

  var config = parseConfig(presets, req.body);

  return generateScreenshot(url, config)
    .then(function (image) {
      var basename = encodeURIComponent(url.replace(/^https?:\/\//, ''));
      var s3Key = presets.getKey
        ? presets.getKey(type, url) : (basename + '.' + type + '.png');
      var storageKey = SCREENSHOTS_BUCKET + '/' + s3Key;
      return objectStore.put(storageKey, image).then(function () {
        return storageKey;
      });
    })
    .then(function (result) {
      res.json({
        location: 'https://s3.amazonaws.com/' + result
      });
      req.logger.info({
        event: 'screenshot-generated',
        duration: Date.now() - startTime
      });
    })
    .catch(next);
});

// Screenshot pages
app.get('/:type(iphone|ipad|desktop)', function (req, res, next) {
  var startTime = Date.now();
  var url = req.query.url;
  var type = req.params.type;

  if (url === undefined) {
    return res.status(400).send('Please provide a url');
  }

  req.logger = req.logger.child({
    url: url,
    type: type
  });

  var presets = types[type];

  var config = parseConfig(presets, req.query);

  return generateScreenshot(url, config)
    .then(function (image) {
      res.contentType('image/png').send(image);
      req.logger.info({
        event: 'screenshot-generated',
        duration: Date.now() - startTime
      });
    })
    .catch(next);
});

app.use(function (err, req, res, next) {
  req.logger.error({
    event: 'app-request-error',
    err: err
  });
  res.sendStatus(500);
});

module.exports = app;
