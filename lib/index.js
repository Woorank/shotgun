var shotgun = require('./shotgun');
var logger = require('./logger');

var PORT = process.env.APP_PORT || process.env.PORT || 3000;

var server = shotgun.listen(PORT, function () {
  logger.info({
    event: 'server-started',
    port: server.address().port
  });
});
