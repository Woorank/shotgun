var bunyan = require('bunyan');

module.exports = bunyan.createLogger({
  name: 'shotgun',
  level: 'info',
  serializers: bunyan.stdSerializers
});
