var Promise = require('bluebird');
var AWS = require('aws-sdk');
var mime = require('mime');
var S3 = Promise.promisifyAll(new AWS.S3());

function parseKey (key) {
  var delimiterPos = key.indexOf('/');
  if (delimiterPos < 0) {
    return { bucket: key, key: '' };
  }
  return {
    bucket: key.substring(0, delimiterPos),
    key: key.substring(delimiterPos + 1)
  };
}

module.exports = {
  get: function (key) {
    var parsed = parseKey(key);
    return S3.getObjectAsync({
      Bucket: parsed.bucket,
      Key: parsed.key
    })
      .then(function (response) {
        return response.Body;
      });
  },
  put: function (key, data) {
    var parsed = parseKey(key);
    return S3.putObjectAsync({
      Bucket: parsed.bucket,
      Key: parsed.key,
      Body: data,
      ContentType: mime.lookup(key)
    })
      .then(function (response) {
        return {};
      });
  }
};
